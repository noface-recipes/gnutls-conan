# gnutls-conan

[Conan.io](https://www.conan.io/) package for [gnutls](https://gnutls.org/).

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[gnutls documentation](https://gnutls.org/documentation.html) for instructions about the library.


## About gnutls


    GnuTLS is a secure communications library implementing the SSL, TLS and DTLS protocols and technologies around them.

See the [library site](https://gnutls.org/) for more info.

## License

GnuTLS is licensed under the 
[GNU Lesser General Public License version 2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) (LGPLv2.1+).
